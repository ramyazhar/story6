from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from ppwstory.models import *
from ppwstory.views import *
from ppwstory.forms import *
from ppwstory.apps import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class ppwstoryUnitTest(TestCase):
    def test_ppwstory_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_ppwstory_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_create_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, create_status)

    def test_model_can_create_new_status(self):
        
        # Creating a new activity
        new_activity = Status.objects.create(title='Name', description='Status')

        # Retrieving all available activity
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_index_contains_greeting(self):
        response = Client().get('/status/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, Apa Kabar?", response_content)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_ppwstory_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('/status/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_ppwstory_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


    

